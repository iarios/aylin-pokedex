import {Image, Text, View, StyleSheet, ScrollView, Alert} from 'react-native';
import * as React from 'react';
import {PokemonButton} from '../components/pokemon-button';
import {getPokemonImage} from '../network/get-pokemon-image';
import {PokemonLogo} from '../components/pokemon-logo';
import {PokemonModel} from '../network/shared';
import {getCapturedPokemons} from '../network/get-captured-pokemons';
import I18n from '../I18n/locales/index';

interface Props {
  readonly navigation: any;
  readonly route: any;
}

interface PokemonItem extends PokemonModel {
  readonly imgURL: string;
}

interface State {
  readonly isLoading: boolean;
  readonly pokemons?: PokemonItem[];
}

export class CapturedPokemonsScreen extends React.Component<Props, State> {
  public constructor(props: Props) {
    super(props);

    this.state = {
      isLoading: true,
    };
  }

  public async componentDidMount() {
    const {capturedPokemons} = await getCapturedPokemons();
    /*extract the list of pokemons (since captured pokemon ID is not needed so far)*/
    const pokemons = capturedPokemons.map(
      capturedPokemon => capturedPokemon.pokemon,
    );

    // build the pokemon item list where the URL is gonna be calculated for each element in the pokemons list passed
    // since we are perfoming an internal API call (async call) we need to add "await"
    const pokemonItems = await this.buildPokemonItems(pokemons);

    this.setState({
      isLoading: false,
      pokemons: pokemonItems,
    });
  }

  private async buildPokemonItems(
    pokemons: PokemonModel[],
  ): Promise<PokemonItem[]> {
    // this returns a Promise that is gonna be handle by "await" whenever it's used t handle it as a synch method
    const pokemonItems = Promise.all(
      // this is necessary to make sure all the API calls are completed "promise all async methods finish"
      pokemons.map(async pokemon => {
        // for every pokemon:
        // calculate the url
        const {imgUrl} = await getPokemonImage(pokemon.id.toString());

        return {
          ...pokemon, // set the prev pokemon props
          imgURL: imgUrl, // set the new prop "url"
        };
      }),
    );

    return pokemonItems; // return the promise
  }

  public render() {
    const {navigation} = this.props;
    const {isLoading, pokemons} = this.state;

    if (isLoading) {
      return <Text> is loading ...</Text>;
    }

    if (!Array.isArray(pokemons) || pokemons.length === 0) {
      return <Text>There are no captured pokemons yet..</Text>;
    }

    return (
      <ScrollView>
        <View>
          <View style={[{alignItems: 'center', flex: 1}]}>
            <PokemonLogo />
          </View>

          {pokemons.map(({name, weight, height, imgURL}, index) => (
            <Text key={index} style={{alignItems: 'flex-start'}}>
              <View style={styles.image}>
                <Image
                  key={index}
                  style={{
                    width: 100,
                    height: 100,
                  }}
                  source={{uri: imgURL}}
                />
              </View>
              <View style={styles.pokemonNameContainer}>
                <Text
                  style={styles.pokemonName}
                  numberOfLines={1}
                  onPress={() => {
                    Alert.alert(name);
                  }}>
                  {name}
                </Text>
              </View>
              <View style={styles.pokemonFieldContainer}>
                <Text style={styles.pokemonField} numberOfLines={1}>
                  w:{weight}
                </Text>
              </View>
              <View style={styles.pokemonFieldContainer}>
                <Text style={styles.pokemonField} numberOfLines={1}>
                  h:{height}
                </Text>
              </View>
            </Text>
          ))}

          <View style={[{margin: 40, flexDirection: 'row'}]}>
            <PokemonButton
              showBorder={false}
              title={I18n.t('title_return')}
              onPress={() => navigation.navigate('Pokemon Adventure')}
            />

            <View style={{width: 10}}></View>

            <PokemonButton
              showBorder={false}
              isDarkMode={false}
              title={I18n.t('title_home')}
              onPress={() => navigation.navigate('Pokemon Welcome')}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  pokemonName: {
    flexDirection: 'column',
    fontSize: 20,
    fontWeight: 'bold',
  },
  pokemonField: {
    fontSize: 20,
    color: 'black',
  },
  image: {
    width: 100,
    height: 70,
  },
  pokemonNameContainer: {
    width: 130,
    maxWidth: 130,
    minWidth: 130,
  },
  pokemonFieldContainer: {
    width: 65,
    maxWidth: 65,
    minWidth: 65,
  },
});
