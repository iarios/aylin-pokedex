import React from 'react';
import {Alert, StyleSheet, Text, View} from 'react-native';

interface FeatureItemProps {
  readonly featureName: string;
  readonly featureValue: string;
}

export function FeatureItem({featureName, featureValue}: FeatureItemProps) {
  return (
    <View style={styles.rootContainer}>
      <Text
        onPress={() => {
          Alert.alert(featureName);
        }}
        numberOfLines={1}
        style={styles.featureName}>
        {featureName}
      </Text>
      <Text
        onPress={() => {
          Alert.alert(featureValue);
        }}
        numberOfLines={1}
        style={styles.featureValue}>
        {featureValue}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  rootContainer: {
    flexDirection: 'row',
    height: 60,
    maxHeight: 60,
    alignItems: 'center',
  },
  featureName: {
    flex: 1,
    color: 'white',
    fontWeight: 'bold',
    marginLeft: 10,
  },
  featureValue: {
    flex: 1,
    color: 'black',
    marginLeft: 10,
  },
});
