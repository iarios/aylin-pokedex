import {BASE_URL, RESOURCE_POKEMON_LIST} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';

interface PokemonListResponse {
  readonly pokemonEntries: string[];
}

export async function getPokemonList(): Promise<PokemonListResponse> {
  const URL = `${BASE_URL}${RESOURCE_POKEMON_LIST}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;
  if (status === HTTP_RESPONSE_OK) {
    // success
    // normalize
    const normalizedResponse = normalizePokemonList(await response.json());

    return normalizedResponse;
  } else {
    // fail
    throw new Error(`Error: List was not found`);
  }
}

function normalizePokemonList({pokemon_entries}): PokemonListResponse {
  const pokemonsNames: string[] = [];
  for (const {pokemon_species} of pokemon_entries) {
    pokemonsNames.push(pokemon_species.name);
  }
  return {
    pokemonEntries: pokemonsNames,
  };
}
