import React from 'react';
import {View, ScrollView, Image} from 'react-native';
import I18n from '../I18n/locales/index';
import {PokemonButton} from '../components/pokemon-button';
import {PokemonLogo} from '../components/pokemon-logo';

interface Props {
  readonly navigation: any;
  readonly route: any;
}

export function PokemonSearchMenuScreen(props: Props) {
  return (
    <ScrollView>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <PokemonLogo />
        <View style={[{margin: 40}]}>
          <Image
            source={require('../resources/search-pikachu.png')}
            style={{width: 400, height: 250}}
          />
        </View>
        <View style={[{margin: 10, flexDirection: 'row'}]}>
          <PokemonButton
            showBorder={true}
            isDarkMode={false}
            title={I18n.t('action_goToPokemonSearch').toUpperCase()}
            onPress={() => props.navigation.navigate('PokemonSearch')}
          />
        </View>

        <View style={[{margin: 10, flexDirection: 'row'}]}>
          <PokemonButton
            showBorder={true}
            isDarkMode={false}
            title={I18n.t('action_goToSearchByArea').toUpperCase()}
            onPress={() => props.navigation.navigate('Areas Menu')}
          />
        </View>
      </View>
    </ScrollView>
  );
}
