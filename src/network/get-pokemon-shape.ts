import {
  BASE_URL,
  RESOURCE_POKEMON_SPECIES_BY_NAME,
  POKEMON_NAME_PARAM,
} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';

interface PokemonShapeResponse {
  readonly shape: string;
}

export async function getPokemonShape(
  name: string,
): Promise<PokemonShapeResponse> {
  const URL = `${BASE_URL}${RESOURCE_POKEMON_SPECIES_BY_NAME.replace(
    POKEMON_NAME_PARAM,
    name,
  )}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;

  if (status === HTTP_RESPONSE_OK) {
    // success
    // normalize
    const normalizedResponse = normalizePokemonShape(await response.json());

    return normalizedResponse;
  } else {
    // fail
    throw new Error(`Error: shape was not found for id: ${name}`);
  }
}

function normalizePokemonShape({
  shape: {name: shapeName},
}): PokemonShapeResponse {
  return {
    shape: shapeName,
  };
}
