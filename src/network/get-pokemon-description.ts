import {
  BASE_URL,
  RESOURCE_POKEMON_SPECIES_BY_NAME,
  POKEMON_NAME_PARAM,
} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';

interface PokemonDescriptionResponse {
  readonly name: string;
  readonly description: string;
}

export async function getPokemonDescription(
  name: string,
): Promise<PokemonDescriptionResponse> {
  const URL = `${BASE_URL}${RESOURCE_POKEMON_SPECIES_BY_NAME.replace(
    POKEMON_NAME_PARAM,
    name,
  )}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;

  if (status === HTTP_RESPONSE_OK) {
    // success
    // normalize
    const jsonResponse = await response.json();
    const normalizedResponse = normalizePokemonDescription(jsonResponse);

    return normalizedResponse;
  } else {
    // fail
    throw new Error(
      `Error: name and description was not found for id: ${name}`,
    );
  }
}

function normalizePokemonDescription({
  name,
  flavor_text_entries,
}): PokemonDescriptionResponse {
  const pokemonDescription: string[] = [];
  for (const {flavor_text} of flavor_text_entries) {
    pokemonDescription.push(flavor_text);
  }
  return {
    description: pokemonDescription[1].replace(/[\n\r]/g, ' '), // TODO analyze array access
    name,
  };
}
