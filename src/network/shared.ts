export interface PokemonModel {
  id: number;
  name: string;
  weight: number;
  height: number;
}
