import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

const imgSize = 150;

interface Props {
  readonly name: string;
  readonly imgUrl: string;
}

export function PokemonNameImage({name, imgUrl}: Props) {
  return (
    <View>
      <Image style={styles.imgContainer} source={{uri: imgUrl}} />
      <Text style={styles.title}>{name}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  imgContainer: {width: imgSize, height: imgSize},
});
