import {View, Image} from 'react-native';
import * as React from 'react';

interface Props {
  readonly width: number;
  readonly height: number;
  readonly isLoading: boolean;
}

export function AdventureLoadingIndicator({
  width: width,
  height: height,
  isLoading,
}: Props) {
  return (
    isLoading && (
      <View style={{alignItems: 'center', marginTop: 100}}>
        <Image
          style={{width, height}}
          source={require('../../resources/encounter-pokemon.gif')}
        />
      </View>
    )
  );
}
