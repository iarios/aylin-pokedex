import {Image, Text, View, StyleSheet, ScrollView} from 'react-native';
import * as React from 'react';
import {PokemonButton} from '../components/pokemon-button';
import {FeaturePanel} from '../components/feature-panel';
import {getPokemonDescription} from '../network/get-pokemon-description';
import {getPokemonSize} from '../network/get-pokemon-size';
import {getPokemonImage} from '../network/get-pokemon-image';
import {Feature} from '../components/feature-panel/feature-panel';
import {getPokemonShape} from '../network/get-pokemon-shape';
import {getPokemonTypes} from '../network/get-pokemon-type';
import {getPokemonHabitat} from '../network/get-pokemon-habitat';
import {getPokemonAbilities} from '../network/get-pokemon-abilities';
import {PokemonLogo} from '../components/pokemon-logo';
import I18n from '../I18n/locales/index';

interface Props {
  readonly navigation: any;
  readonly route: any;
}

interface PokemonInfo {
  readonly description: string;
  readonly features: Feature[];
  readonly imgURL: string;
  readonly name: string;
}

interface State {
  readonly isLoading: boolean;
  readonly pokemonInfo?: PokemonInfo;
}

export class PokemonDetailsScreen extends React.Component<Props, State> {
  private pokemonName: string = '';

  public constructor(props: Props) {
    super(props);

    this.state = {
      isLoading: true,
    };
  }

  public async componentDidMount() {
    this.pokemonName = this.props.route.params?.pokemonName;

    console.log(`this.pokemonName: ${this.pokemonName}`);

    // TODO delete this hardcoded variable when prev screen is ready
    const {description} = await getPokemonDescription(this.pokemonName);
    console.log(`description: ${description}`);

    const {name} = await getPokemonDescription(this.pokemonName);
    console.log(`description: ${name}`);

    const {height} = await getPokemonSize(this.pokemonName);
    console.log(`height: ${height}`);

    const {weight} = await getPokemonSize(this.pokemonName);
    console.log(`weight: ${weight}`);

    const {shape} = await getPokemonShape(this.pokemonName);
    console.log(`shape: ${shape}`);

    const {types} = await getPokemonTypes(this.pokemonName);
    console.log(`type: ${types}`);

    const {habitat} = await getPokemonHabitat(this.pokemonName);
    console.log(`habitat: ${habitat}`);

    const {abilities} = await getPokemonAbilities(this.pokemonName);
    console.log(`abilities: ${abilities}`);

    const {imgUrl} = await getPokemonImage(this.pokemonName);
    console.log(`imgUrl: ${imgUrl}`);

    /* consume other endpoints to retrieve pokemon data in the screen */
    /* init state with response data */

    // request finished
    this.setState({
      isLoading: false,
      pokemonInfo: {
        name,
        description,
        features: [
          {
            name: I18n.t('title_height'),
            value: height,
          },
          {
            name: I18n.t('title_weight'),
            value: weight,
          },
          {
            name: I18n.t('title_shape'),
            value: shape,
          },
          {
            name: I18n.t('title_habitat'),
            value: habitat,
          },
          {
            name: I18n.t('title_abilities'),
            value: abilities.toString(),
          },
          {
            name: I18n.t('title_types'),
            value: types.toString(),
          },
        ],
        imgURL: imgUrl,
      },
    });
  }

  render() {
    const {navigation} = this.props;
    const {isLoading, pokemonInfo} = this.state;

    if (isLoading) {
      return <Text> is loading ...</Text>;
    }

    if (!pokemonInfo) {
      return <Text> Pokemon info is not available.</Text>;
    }

    const {name, description, features, imgURL} = pokemonInfo;

    return (
      <ScrollView>
        <View>
          <View style={[{alignItems: 'center', flex: 1}]}>
            <PokemonLogo />
          </View>

          <Text style={styles.title}>{name}</Text>
          <Text style={styles.description}>{description}</Text>

          <View
            style={{margin: 15, flexDirection: 'row', alignItems: 'center'}}>
            <Image
              style={{
                width: 100,
                height: 100,
              }}
              source={{uri: imgURL}}
            />
            <FeaturePanel features={features} />
          </View>

          <View style={[{margin: 10, flexDirection: 'row'}]}>
            <PokemonButton
              showBorder={true}
              isDarkMode={false}
              title={I18n.t('action_viewEvolution')}
              onPress={() =>
                navigation.navigate('Evolution Details', {
                  pokemonName: this.pokemonName,
                })
              }
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  description: {
    fontSize: 18,
    alignSelf: 'center',
  },
});
