import {
  BASE_URL,
  RESOURCE_POKEMON_SPECIES_BY_NAME,
  POKEMON_NAME_PARAM,
} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';

interface PokemonHabitatResponse {
  readonly habitat: string;
}

export async function getPokemonHabitat(
  name: string,
): Promise<PokemonHabitatResponse> {
  const URL = `${BASE_URL}${RESOURCE_POKEMON_SPECIES_BY_NAME.replace(
    POKEMON_NAME_PARAM,
    name,
  )}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;

  if (status === HTTP_RESPONSE_OK) {
    // success
    // normalize
    const normalizedResponse = normalizePokemonHabitat(await response.json());

    // return pokemon Habitat
    return normalizedResponse;
  } else {
    // fail
    throw new Error(`Error: Habitat was not found for id: ${name}`);
  }
}

function normalizePokemonHabitat({
  habitat: {name: habitatName},
}): PokemonHabitatResponse {
  return {
    habitat: habitatName,
  };
}
