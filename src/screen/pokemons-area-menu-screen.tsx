import {FlatList, StyleSheet, Text, View} from 'react-native';
import * as React from 'react';
import {PokemonLogo} from '../components/pokemon-logo';

interface Props {
  readonly navigation: any;
}

interface State {
  readonly pokemons: string[];
}

const areas = ['forest', 'field', 'mountain', 'pond', 'sea'];

export class PokemonsAreaMenuScreen extends React.Component<Props, State> {
  public constructor(props: Props) {
    super(props);

    this.state = {
      pokemons: [],
    };
  }

  public render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <View style={[{margin: 10}]}>
          <PokemonLogo />

          <FlatList
            data={areas}
            renderItem={({item}) => {
              return (
                <View style={styles.container}>
                  <Text
                    style={styles.text}
                    onPress={() =>
                      this.props.navigation.navigate('Pokemons Area', {
                        pokemonArea: item,
                      })
                    }>
                    {item.toUpperCase()}
                  </Text>
                </View>
              );
            }}
            keyExtractor={item => {
              return item;
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: 40,
    fontWeight: 'bold',
    alignSelf: 'center',
    fontFamily: 'i',
    color: '#2e10a7',
  },
  container: {
    flex: 1,
    margin: 10,
    backgroundColor: '#f2cb43',
    borderRadius: 30,
  },
});
