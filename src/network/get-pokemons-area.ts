import {BASE_URL, RESOURCE_POKEMONS_AREA} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';

export interface PokemonsByArea {
  readonly pokemonName: string;
}
interface PokemonsAreaResponse {
  readonly pokemonEncounters: PokemonsByArea[];
}

export async function getPokemonsArea(
  name: string,
): Promise<PokemonsAreaResponse> {
  const URL = `${BASE_URL}${RESOURCE_POKEMONS_AREA}${name}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;
  if (status === HTTP_RESPONSE_OK) {
    const jsonResponse = await response.json();
    const normalizedResponse = normalizePokemonsArea(jsonResponse);
    return normalizedResponse;
  } else {
    throw new Error(`Error: Abilities was not found for id: ${name}`);
  }
}

function normalizePokemonsArea({pokemon_encounters}): PokemonsAreaResponse {
  return {
    pokemonEncounters: Array.isArray(pokemon_encounters)
      ? pokemon_encounters.map(({pokemon_species: {name}}) => {
          return {
            pokemonName: name,
          };
        })
      : [],
  };
}
