import {HTTP_RESPONSE_OK} from '../config/http-request-status';
import {
  BASE_URL,
  RESOURCE_POKEMON_EVOLUTION,
  EVOLUTION_ID_PARAM,
} from '../config/routes';

interface PokemonEvolutionResponse {
  readonly evolutionName: string;
}

export async function getPokemonEvolution(
  id: string,
): Promise<PokemonEvolutionResponse> {
  const URL = `${BASE_URL}${RESOURCE_POKEMON_EVOLUTION.replace(
    EVOLUTION_ID_PARAM,
    id,
  )}`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;
  if (status === HTTP_RESPONSE_OK) {
    // success
    // normalize
    const jsonResponse = await response.json();
    const normalizedResponse = normalizePokemonEvolution(jsonResponse);
    // return pokemon Evolution
    return normalizedResponse;
  } else {
    // fail
    throw new Error(`Error: Evolution was not found for id: ${id}`);
  }
}

function normalizePokemonEvolution({chain}): PokemonEvolutionResponse {
  return {
    evolutionName: chain.evolves_to[0].species.name, // TODO - analyze array access
  };
}
