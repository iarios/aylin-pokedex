import React from 'react';
import {Image, TouchableOpacity} from 'react-native';

interface Props {
  readonly onPress: () => void;
}

const pokebolaSize = 110;

export function PokemonPokebola({onPress}: Props) {
  return (
    <TouchableOpacity onPress={onPress}>
      <Image
        style={{
          width: pokebolaSize,
          height: pokebolaSize,
        }}
        source={require('../../resources/pokeball.png')}
      />
    </TouchableOpacity>
  );
}
