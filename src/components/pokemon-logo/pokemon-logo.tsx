import React from 'react';
import {View, Image} from 'react-native';

export function PokemonLogo() {
  return (
    <View style={[{margin: 15}]}>
      <Image
        source={require('../../resources/logo-pokemon.png')}
        style={{width: 410, height: 150}}
      />
    </View>
  );
}
