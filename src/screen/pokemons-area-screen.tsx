import {Image, Text, View, StyleSheet, ScrollView, Alert} from 'react-native';
import * as React from 'react';
import {PokemonButton} from '../components/pokemon-button';
import {getPokemonImage} from '../network/get-pokemon-image';
import {PokemonLogo} from '../components/pokemon-logo';
import I18n from '../I18n/locales/index';
import {getPokemonsArea, PokemonsByArea} from '../network/get-pokemons-area';

interface Props {
  readonly navigation: any;
  readonly route: any;
}

interface PokemonItem extends PokemonsByArea {
  readonly imgURL: string;
}

interface State {
  readonly isLoading: boolean;
  readonly pokemons?: PokemonItem[];
}

export class PokemonsAreaScreen extends React.Component<Props, State> {
  private pokemonArea: string = '';

  public constructor(props: Props) {
    super(props);

    this.state = {
      isLoading: true,
    };
  }

  public async componentDidMount() {
    this.pokemonArea = this.props.route.params?.pokemonArea;
    const {pokemonEncounters} = await getPokemonsArea(this.pokemonArea);
    const pokemons = pokemonEncounters.map(pokemonsArea => pokemonsArea);
    const pokemonItems = await this.buildPokemonItems(pokemons);

    this.setState({
      isLoading: false,
      pokemons: pokemonItems,
    });
  }

  private async buildPokemonItems(
    pokemons: PokemonsByArea[],
  ): Promise<PokemonItem[]> {
    const pokemonItems = Promise.all(
      pokemons.map(async pokemon => {
        const {imgUrl} = await getPokemonImage(pokemon.pokemonName.toString());
        return {
          ...pokemon,
          imgURL: imgUrl,
        };
      }),
    );
    return pokemonItems;
  }

  public render() {
    const {navigation} = this.props;
    const {isLoading, pokemons} = this.state;

    if (isLoading) {
      return <Text> is loading ...</Text>;
    }

    if (!Array.isArray(pokemons) || pokemons.length === 0) {
      return <Text>There are not pokemons yet..</Text>;
    }

    return (
      <ScrollView>
        <View>
          <View style={[{alignItems: 'center', flex: 1}]}>
            <PokemonLogo />
          </View>
          <View style={{alignItems: 'center'}}>
            <Text style={{fontSize: 30, fontWeight: 'bold'}}>
              {I18n.t('title_area').toUpperCase()}{' '}
              {this.pokemonArea.toUpperCase()}
            </Text>
          </View>
          {pokemons.map(({pokemonName, imgURL}, index) => (
            <Text key={index} style={{alignItems: 'flex-start'}}>
              <View style={styles.image}>
                <Image
                  key={index}
                  style={{
                    width: 100,
                    height: 100,
                  }}
                  source={{
                    uri: imgURL,
                  }}
                />
              </View>
              <View style={styles.pokemonNameContainer}>
                <Text
                  style={styles.pokemonName}
                  numberOfLines={1}
                  onPress={() => {
                    Alert.alert(pokemonName.toUpperCase());
                  }}>
                  {pokemonName.toUpperCase()}
                </Text>
              </View>
            </Text>
          ))}

          <View style={[{margin: 40, flexDirection: 'row'}]}>
            <PokemonButton
              showBorder={false}
              title={I18n.t('title_return')}
              onPress={() => navigation.navigate('Areas Menu')}
            />

            <View style={{width: 10}}></View>

            <PokemonButton
              showBorder={false}
              isDarkMode={false}
              title={I18n.t('title_home')}
              onPress={() => navigation.navigate('Pokemon Welcome')}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  pokemonName: {
    flexDirection: 'column',
    fontSize: 20,
    fontWeight: 'bold',
  },
  image: {
    width: 100,
    height: 70,
  },
  pokemonNameContainer: {
    width: 130,
    maxWidth: 130,
    minWidth: 130,
  },
});
