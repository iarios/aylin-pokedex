export default {
  // translationID: translationValue
  title_pokedex: 'POKEDEX',
  title_greeting: 'Hello world',
  action_ok: 'Ok',
  detail_whatsIsReact: 'React is a library...',
  title_height: 'Height',
  title_weight: 'Weight',
  title_shape: 'Shape',
  title_habitat: 'Habitat',
  title_abilities: 'Abilities',
  title_types: 'Types',
  action_goToAdventureSection: 'Adventure',
  action_encounter: 'Encounter Pokemon',
  action_seeCapturedPokemons: 'View my Pokemons',
  title_captureAttempts: 'Capture attempts:',
  title_return: 'Return',
  title_home: 'Home',
  action_viewEvolution: 'View evolution',
  action_pokemonSearch: 'Search',
  detail_pokemonNotEncountered: 'Pokemon not encountered',
  title_pokemonCaptured: 'Captured',
  title_capturedPokemon: 'Captured Pokemon',
  title_pokemonNotCaptured: 'Not captured',
  detail_pokemonNotCaptured: 'Not Captured Pokemon',
  title_languageSelection: 'Language Selection',
  title_translate: 'Translate',
  title_textSpanish: 'Spanish',
  title_textEnglish: 'English',
  title_textCancel: 'Cancel',
  action_clickChangeLanguage: 'Click to Change Language',
  action_goToSearchByArea: 'Pokemon areas',
  action_goToPokemonSearch: 'Pokemon Search',
  title_area: 'Area',
};
