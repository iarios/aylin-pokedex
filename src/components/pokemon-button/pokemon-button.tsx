import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';

interface Props {
  /* isDarkMode
   * if TRUE ->
   *  text = light
   *  background = dark
   *  border = light
   * if FALSE ->
   *  text = dark
   *  background = light
   *  border = dark
   * */
  readonly isDarkMode?: boolean;
  readonly showBorder?: boolean;

  readonly title: string;
  readonly onPress: () => void;
}

export function PokemonButton({
  title,
  onPress,
  isDarkMode = true,
  showBorder = true,
}: Props) {
  return (
    <TouchableOpacity
      style={[
        styles.btnContainer,
        showBorder && isDarkMode ? styles.btnBorderLight : styles.btnBorderDark,
        showBorder ? styles.btnBorder : undefined,
        isDarkMode ? styles.btnContainerDark : styles.btnContainerLight,
      ]}
      onPress={onPress}>
      <Text
        style={[
          styles.btnText,
          isDarkMode ? styles.btnTextLight : styles.btnTextDark,
        ]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  btnTextLight: {
    color: 'white',
  },
  btnTextDark: {
    color: 'black',
  },
  btnText: {
    fontSize: 25,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  btnContainerLight: {
    backgroundColor: '#f2cb43',
  },
  btnContainerDark: {
    backgroundColor: '#2e10a7',
  },
  btnContainer: {
    flex: 1,
    padding: 6,
    borderRadius: 10,
  },
  btnBorderLight: {
    borderColor: '#f2cb43',
  },
  btnBorderDark: {
    borderColor: '#2e10a7',
  },
  btnBorder: {
    borderWidth: 10,
  },
});
