import * as React from 'react';
import {Button, View, ScrollView, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {getPokemonColor} from '../network/get-pokemon-color';
import {getPokemonDescription} from '../network/get-pokemon-description';
import {getPokemonHabitat} from '../network/get-pokemon-habitat';
import {getPokemonAbilities} from '../network/get-pokemon-abilities';
import {getPokemonList} from '../network/get-pokemon-list';
import {getPokemonImage} from '../network/get-pokemon-image';
import {getPokemonTypes} from '../network/get-pokemon-type';
import {getPokemonSize} from '../network/get-pokemon-size';
import {getPokemonShape} from '../network/get-pokemon-shape';
import {PokemonWelcomeScreen} from './pokemon-welcome-screen';
import {PokemonDetailsScreen} from './pokemon-details-screen';
import {PokemonSearchScreen} from './pokemon-search-screen';
import {PokemonEvolutionScreen} from './pokemon-evolution-screen';
import {HomeScreen} from './home-screen';
import {getPokemonEvolution} from '../network/get-pokemon-evolution';
import {PokemonNameImage} from '../components/pokemon-name-image/index';
import {PokemonButton} from '../components/pokemon-button/index';
import {PokemonAdventureScreen} from './pokemon-adventure-screen';
import {PokemonPokebola} from '../components/pokemon-pokebola';
import {encounterPokemonRequest} from '../network/encounter-pokemon-request';
import {capturePokemon} from '../network/capture-pokemon';
import {PokemonVisibility} from '../components/pokemon-visibility';
import {getCapturedPokemons} from '../network/get-captured-pokemons';
import {CapturedPokemonsScreen} from './captured-pokemons-screen';
import {getPokemonsArea} from '../network/get-pokemons-area';
import {PokemonsAreaScreen} from './pokemons-area-screen';
import {PokemonSearchMenuScreen} from './pokemon-search-menu-screen';
import {PokemonsAreaMenuScreen} from './pokemons-area-menu-screen';

const pokemonVisibilityStories = [3, 2, 1, 10];

const btnStories = [
  {
    title: 'get captured pokemons',
    onPress: async () => {
      const response = await getCapturedPokemons();
      console.log(`Pokemons: ${JSON.stringify(response.capturedPokemons)} `);
    },
  },
  {
    title: 'get pokemon encounter',
    onPress: async () => {
      const response = await encounterPokemonRequest();
      console.log(
        `encounter: ${response.encounter}, encounterToken: ${
          response.encounterToken
        }, pokemon: ${JSON.stringify(response.pokemon)}`,
      );
    },
  },
  {
    title: 'get pokemon captured',
    onPress: async () => {
      const response = await capturePokemon('40bb8c6');
      console.log(
        `Captured: ${response.captured}, pokemon: ${JSON.stringify(
          response.pokemon,
        )}, remainingAttempts: ${response.remainingAttempts}`,
      );
    },
  },
  {
    title: 'get pokemon color',
    onPress: async () => {
      const response = await getPokemonColor('1');
      console.log(`color: ${response.color}`);
    },
  },
  {
    title: 'get pokemon habitat',
    onPress: async () => {
      const response = await getPokemonHabitat('charmander');
      console.log(`habitat: ${response.habitat}`);
    },
  },
  {
    title: 'get pokemon abilities',
    onPress: async () => {
      const response = await getPokemonAbilities('charmander');
      console.log(`abilities: ${response.abilities}`);
    },
  },
  {
    title: 'get pokemon name and description',
    onPress: async () => {
      const response = await getPokemonDescription('charmander');
      console.log(
        `Name: ${response.name} Description: ${response.description}`,
      );
    },
  },
  {
    title: 'get pokemon shape',
    onPress: async () => {
      const response = await getPokemonShape('ditto');
      console.log(`Shape: ${response.shape}`);
    },
  },
  {
    title: 'get pokemon evolution',
    onPress: async () => {
      const response = await getPokemonEvolution('2');
      console.log(`evolution: ${response.evolutionName}`);
    },
  },
  {
    title: 'get pokemon List',
    onPress: async () => {
      const response = await getPokemonList();
      console.log(`Pokemon: ${response.pokemonEntries}`);
    },
  },
  {
    title: 'get pokemon image',
    onPress: async () => {
      const response = await getPokemonImage('ditto');
      console.log(`Image: ${response.imgUrl}`);
    },
  },
  {
    title: 'get pokemon types',
    onPress: async () => {
      const response = await getPokemonTypes('charmander');
      console.log(`types: ${response.types}`);
    },
  },
  {
    title: 'get pokemon weigth and height',
    onPress: async () => {
      const response = await getPokemonSize('venusaur');
      console.log(`Heigth: ${response.height} Weigth: ${response.weight}`);
    },
  },
  {
    title: 'get pokemons area',
    onPress: async () => {
      const response = await getPokemonsArea('sea');
      console.log(`Names: ${response.pokemon_encounters}`);
    },
  },
];

interface EndpointsScreenProps {
  readonly navigation: any;
}

function EndpointsScreen(props: EndpointsScreenProps) {
  return (
    <ScrollView style={{flex: 1}}>
      <View style={[{margin: 10}]}>
        <PokemonNameImage
          name="ditto"
          imgUrl={
            'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/132.png'
          }
        />
      </View>
      <View>
        {btnStories.map(({title, onPress}, index) => {
          return (
            <ButtonStoryContainer key={index} title={title} onPress={onPress} />
          );
        })}
      </View>

      <View style={[{margin: 10}]}>
        <Button
          title="Details"
          onPress={() => props.navigation.navigate('Pokemon Details')}
        />
      </View>

      <View>
        <Text>darkMode and border</Text>
        <PokemonButton
          title="POKEDEX (default)"
          onPress={() => {
            console.log('onPress');
          }}
        />

        <View style={{height: 10}}></View>

        <Text>lightMode and border</Text>
        <PokemonButton
          isDarkMode={false}
          title="POKEDEX (default)"
          onPress={() => {
            console.log('onPress');
          }}
        />

        <View style={{height: 10}}></View>

        <Text>darkMode</Text>
        <PokemonButton
          showBorder={false}
          title="POKEDEX"
          onPress={() => {
            console.log('onPress');
          }}
        />
        <View style={{height: 10}}></View>

        <Text>lightMode</Text>
        <PokemonButton
          showBorder={false}
          isDarkMode={false}
          title="POKEDEX"
          onPress={() => {
            console.log('onPress');
          }}
        />

        <View style={{alignItems: 'center'}}>
          <PokemonPokebola onPress={() => console.log('Capture Pokemon')} />
        </View>
      </View>

      <View style={[{margin: 10}]}>
        {pokemonVisibilityStories.map((attemps, index) => (
          <PokemonVisibility
            imgURL={
              'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/132.png'
            }
            captureAttempts={attemps}
            key={index}
          />
        ))}
      </View>
    </ScrollView>
  );
}

const Stack = createStackNavigator();

function Navegation() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Endpoints" component={EndpointsScreen} />
        <Stack.Screen name="Pokemon Welcome" component={PokemonWelcomeScreen} />
        <Stack.Screen name="PokemonSearch" component={PokemonSearchScreen} />
        <Stack.Screen name="Pokemon Details" component={PokemonDetailsScreen} />
        <Stack.Screen
          name="Pokemon Adventure"
          component={PokemonAdventureScreen}
        />
        <Stack.Screen
          name={'Evolution Details'}
          component={PokemonEvolutionScreen}
        />
        <Stack.Screen
          name="Captured Pokemons"
          component={CapturedPokemonsScreen}
        />
        <Stack.Screen name={'Pokemons Area'} component={PokemonsAreaScreen} />
        <Stack.Screen
          name={'Pokemons Menu'}
          component={PokemonSearchMenuScreen}
        />
        <Stack.Screen name={'Areas Menu'} component={PokemonsAreaMenuScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

interface ButtonStoryContainerProps {
  readonly title: string;
  readonly onPress: () => void;
}

export function ButtonStoryContainer({
  title,
  onPress,
}: ButtonStoryContainerProps) {
  return (
    <View style={[{margin: 10}]}>
      <Button title={title} onPress={onPress} />
    </View>
  );
}

export default Navegation;
