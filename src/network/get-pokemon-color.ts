import {BASE_URL, RESOURCE_POKEMON_SPECIES} from '../config/routes';
import {HTTP_RESPONSE_OK} from '../config/http-request-status';

interface PokemonColorResponse {
  readonly color: string;
}

export async function getPokemonColor(
  id: string,
): Promise<PokemonColorResponse> {
  const URL = `${BASE_URL}${RESOURCE_POKEMON_SPECIES}${id}/`;
  console.log(`URL: ${URL}`);

  const response = await fetch(URL, {
    method: 'GET',
  });

  const status = response.status;

  if (status === HTTP_RESPONSE_OK) {
    // success

    // normalize
    const normalizedResponse = normalizePokemonColor(await response.json());

    // return pokemon color
    return normalizedResponse;
  } else {
    // fail
    throw new Error(`Error: color was not found for id: ${id}`);
  }
}

function normalizePokemonColor({
  color: {name: colorName},
}): PokemonColorResponse {
  return {
    color: colorName,
  };
}
